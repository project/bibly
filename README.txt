bib.ly finds Bible references on your site, creates a link that lets users
select their desired website (YouVersion, BibleGateway, etc.), and shows the
text in a hover box.

Check out http://bib.ly for more information.
<?php

/**
 * @file
 * Integrate Drupal with the service at http://bib.ly.
 */

/**
 * Implements hook_menu().
 */
function bibly_menu() {
  $items = array();
  $items['admin/config/content/bibly'] = array(
    'title' => 'bibly',
    'description' => 'Bible reference shortner settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bibly_admin_form'),
    'access arguments' => array('administer bibly'),
    'file' => 'bibly.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function bibly_permission() {
  return array(
    'administer bibly' => array(
      'title' => t('Administer bibly'),
    )
  );
}

/**
 * Implements hook_init().
 *
 * Add the bib.ly JS and CSS files to every page.
 */
function bibly_init() {
  // @todo Switch to local coppies after features are complete.
  // Tap into the jquery_update module to know if minified or full js file should be used.
  $min = variable_get('jquery_update_compression_type', 'min') == 'none' ? '' : '.min';

  // The styling CSS.
  $options = array(
    'type' => 'external',
    'group' => CSS_SYSTEM,
    'every_page' => TRUE,
    'scope' => variable_get('bibly_location', 'footer'),
  );
  drupal_add_css('http://code.bib.ly/bibly' . $min . '.css', $options);

  // The JS to make the magic happen.
  $options = array(
    'type' => 'external',
    'group' => JS_LIBRARY,
    'every_page' => TRUE,
    'scope' => variable_get('bibly_location', 'footer'),
  );
  drupal_add_js('http://code.bib.ly/bibly' . $min . '.js', $options);

  // The config options.
  $startNodeId = variable_get('bibly_startNodeId', '');
  $linkVersion = variable_get('bibly_linkVersion', '');
  $classname = variable_get('bibly_classname', '');
  $options = array(
    'type' => 'inline',
    'group' => JS_LIBRARY,
    'every_page' => TRUE,
    'scope' => variable_get('bibly_location', 'footer'),
  );
  $data = (!empty($startNodeId) ? "bibly.startNodeId = '". check_plain($startNodeId) ."';\n" : '')
          ."bibly.enablePopups = ". (variable_get('bibly_enablePopups', 1) ? 'true' : 'false') .";\n"
          ."bibly.popupVersion = '" . variable_get('bibly_popupVersion', 'NET') . "';\n"
          .(!empty($linkVersion) ? "bibly.linkVersion = '" . $linkVersion . "';\n" : '')
          .(!empty($classname) ? "bibly.className = '". check_plain($classname) ."';\n" : '');
  drupal_add_js($data, $options);
}
